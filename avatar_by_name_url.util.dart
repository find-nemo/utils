String getAvatarUrlByName(String name) {
  return name.isEmpty ? '' : 'https://ui-avatars.com/api/?name=$name';
}
