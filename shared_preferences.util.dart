import 'package:shared_preferences/shared_preferences.dart';

class SharedPreferencesUtil {
  // Singleton
  static final SharedPreferencesUtil _providerStateClient =
      SharedPreferencesUtil._internal();

  factory SharedPreferencesUtil() => _providerStateClient;

  // private constructor
  SharedPreferencesUtil._internal();

  static SharedPreferencesUtil get shared => _providerStateClient;

  SharedPreferences sharedPreferences;

  Future<SharedPreferences> fetchSharedPreference() async {
    sharedPreferences = await SharedPreferences.getInstance();
    return sharedPreferences;
  }
}
