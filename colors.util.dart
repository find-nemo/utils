import 'package:flutter/material.dart';

const oceanBlue900 = const Color(0xFF1C293C);
const oceanBlue800 = const Color(0xFF2F3D54); // Primary Color
const oceanBlue700 = const Color(0xFF3E4F6A);
const oceanBlue600 = const Color(0xFF4E6281);
const oceanBlue500 = const Color(0xFF5C7091);
const oceanBlue400 = const Color(0xFF7385A3);
const oceanBlue300 = const Color(0xFF8A9AB6);
const oceanBlue200 = const Color(0xFFA9B7CF);
const oceanBlue100 = const Color(0xFFC7D4E8);
const oceanBlue50 = const Color(0xFFE8EDFE);

const uniqueGreen900 = const Color(0xFF00592D);
const uniqueGreen800 = const Color(0xFF007748);
const uniqueGreen700 = const Color(0xFF008756);
const uniqueGreen600 = const Color(0xFF009865);
const uniqueGreen500 = const Color(0xFF00A571);
const uniqueGreen400 = const Color(0xFF00B584);
const uniqueGreen300 = const Color(0xFF22C399); // Secondary Color
const uniqueGreen200 = const Color(0xFF72D5B6);
const uniqueGreen100 = const Color(0xFFACE5D3);
const uniqueGreen50 = const Color(0xFFDDF5EE);

const whiteSmoke = const Color(0xFFF5F5F5);
const white = const Color(0xFFFFFFFF);
const black = const Color(0x00000000);
const grey = const Color(0xFFC4C4C4);

const cavemanBackgorund = const Color(0xFFF0F4F7);
