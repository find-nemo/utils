String allWordsCapitilize(String str) {
  return str.toLowerCase().split(' ').map((word) {
    if (word.length == 0) return '';
    String leftText = (word.length > 1) ? word.substring(1, word.length) : '';
    return word[0].toUpperCase() + leftText;
  }).join(' ');
}
