import 'dart:io' show Platform;

import 'package:device_info/device_info.dart';
import 'package:f_logs/f_logs.dart';
import 'package:nemoparent/utils/push_notification.util.dart';
import 'package:openapi/api.dart';

class DeviceInfoHelper {
  Future<CreateDeviceBody> getDeviceResponse() async {
    try {
      DeviceInfoPlugin deviceInfoPlugin = DeviceInfoPlugin();
      CreateDeviceBody createDeviceBody = CreateDeviceBody();
      if (Platform.isIOS) {
        IosDeviceInfo iosDeviceInfo = await deviceInfoPlugin.iosInfo;
        createDeviceBody.uId = iosDeviceInfo.identifierForVendor;
        createDeviceBody.model = iosDeviceInfo.model;
        createDeviceBody.make = iosDeviceInfo.systemName;
      } else if (Platform.isAndroid) {
        AndroidDeviceInfo androidDeviceInfo =
            await deviceInfoPlugin.androidInfo;
        createDeviceBody.uId = androidDeviceInfo.androidId;
        createDeviceBody.model = androidDeviceInfo.model;
        createDeviceBody.make = androidDeviceInfo.brand;
      }
      PushNotificationService pushNotificationService =
          PushNotificationService();
      createDeviceBody.fcmToken = pushNotificationService.shared.fcmToken;
      return createDeviceBody;
    } catch (e) {
      FLog.logThis(text: e.toString(), type: LogLevel.ERROR);
      return null;
    }
  }
}
