import 'package:openapi/api.dart';

dynamic deserializeError(dynamic e) {
  if (e != null && e.message != null) {
    try {
      ApiClient apiClient = ApiClient();
      return apiClient.deserialize(e.message, 'ErrorResponse') as ErrorResponse;
    } catch (e) {
      return e;
    }
  }
}
