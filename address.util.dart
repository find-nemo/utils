import 'package:nemoparent/models/address.model.dart' as AddressModel;
import 'package:geocoder/geocoder.dart';
import 'package:nemoparent/utils/string_manipulation.util.dart';

AddressModel.Address getAddressFromGeolocationAddress(
    Address address, double latitude, double longitude) {
  return AddressModel.Address(
      city: address.locality,
      state: address.adminArea,
      postalCode: address.postalCode,
      country: address.countryName,
      latitude: latitude,
      longitude: longitude,
      address1: "${address.subThoroughfare} ${address.thoroughfare}",
      address2: address.subLocality,
      address3: address.subAdminArea);
}

String prettifyAddress(dynamic address) {
  String temp = '';
  if (address == null) {
    return temp;
  }
  if (address.address1 != null && address.address1.toLowerCase() != "null") {
    temp += address.address1 + ", ";
  }
  if (address.address2 != null && address.address2.toLowerCase() != "null") {
    temp += address.address2 + ", ";
  }
  if (address.address3 != null && address.address3.toLowerCase() != "null") {
    temp += address.address3 + ", ";
  }
  if (address.city != null && address.city.toLowerCase() != "null") {
    temp += address.city + ", ";
  }
  if (address.state != null && address.state.toLowerCase() != "null") {
    temp += address.state + ", ";
  }
  if (address.postalCode != null &&
      address.postalCode.toLowerCase() != "null") {
    temp += address.postalCode;
  }
  temp = temp.replaceAll("Null", '');
  temp = temp.replaceAll("null", '');
  return allWordsCapitilize(temp.trim());
}
