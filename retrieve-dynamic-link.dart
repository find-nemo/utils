import 'package:firebase_dynamic_links/firebase_dynamic_links.dart';
import 'package:flutter/material.dart';
import 'package:nemoparent/utils/route_arguments.util.dart';
import 'package:nemoparent/utils/routes.util.dart';

Future<void> retrieveDynamicLink(BuildContext context) async {
  try {
    final PendingDynamicLinkData data =
        await FirebaseDynamicLinks.instance.getInitialLink();
    _onLink(context, data);

    FirebaseDynamicLinks.instance.onLink(
        onSuccess: (PendingDynamicLinkData dynamicLink) async {
      _onLink(context, dynamicLink);
    });
  } catch (e) {
    print(e.toString());
  }
}

void _onLink(BuildContext context, PendingDynamicLinkData dynamicLink) {
  final Uri deepLink = dynamicLink?.link;
  if (deepLink != null) {
    if (deepLink.queryParameters.containsKey('public-id')) {
      String id = deepLink.queryParameters['public-id'];
      Navigator.pushNamed(context, LINK_BY_GROUP_ID,
          arguments: SelectedArguments(publicId: id));
    }
  }
}
