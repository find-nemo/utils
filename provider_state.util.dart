import 'dart:async';

import 'package:nemoparent/data_access/firestore_collection.da.dart';
import 'package:nemoparent/data_access/firestore_collection.interface.dart';

class ProviderStateClient {
  FirestoreCollectionDataAccessInterface
      firestoreCollectionDataAccessInterface = FirestoreCollectionDataAccess();

  // Singleton
  static final ProviderStateClient _providerStateClient =
      ProviderStateClient._internal();

  factory ProviderStateClient() => _providerStateClient;

  // private constructor
  ProviderStateClient._internal();

  static ProviderStateClient get shared => _providerStateClient;

  String currentUserPhoneNumber;
  int currentUserId;
  int deviceId;
  double latitude;
  double longitude;
  bool isListeningDataChanges = false;
  StreamController _firestoreUserController = StreamController.broadcast();

  Stream get firestoreUserController => _firestoreUserController.stream;

  void listenUserChanges() {
    if (isListeningDataChanges) {
      return;
    }
    isListeningDataChanges = true;
    firestoreCollectionDataAccessInterface
        .streamLastDataTimestamp(userId: currentUserId)
        .listen((event) => _firestoreUserController.add(event));
  }

  void stopListenUserChanges() {
    _firestoreUserController.close();
    isListeningDataChanges = false;
  }
}
