class SelectedArguments {
  final int childId;
  final int userId;
  final String publicId;
  final int addressId;

  SelectedArguments({this.childId, this.userId, this.publicId, this.addressId});
}
