const String APP_NAME = 'Nemo';
const String PHONE_NUMBER_PREFIX = '+91';
const int PROVIDER_REFRESH_SECONDS = 30;
const int PROVIDER_REFRESH_FIREBASE_TOKEN_MINUTES = 10;

const String PROFILE_IMAGE_PLACEHOLDER =
    'assets/images/blank-profile-picture.png';

const String CREATE_PROFILE = 'Create Profile';
const String CREATE_CHILD = 'Create Child';
const String ENTER_CHILD_NAME = 'Enter your child fullname';
const String ENTER_FULL_NAME = 'Enter your fullname';
const String NO_GROUPS_TEXT =
    "No child found. Please contact your driver or contact us at info@findnemo.in to get started. If you have already contacted us, please stay put -- we are working on your request and will contact you as soon as it's completed.";

const String PRIVACY_POLICY_URL =
    'https://gitlab.com/find-nemo/licenses/-/raw/main/nemo_parent/privacy_policy.md';
const String TERMS_AND_CONDITION_URL =
    "https://gitlab.com/find-nemo/licenses/-/raw/main/nemo_parent/terms_and_conditions.md";

const String EMAIL_ADDRESS = "info@findnemo.in";
const String CONTACT_US_EMAIL_BODY_TEXT = 'Please type your message here:';
const String CONTACT_US_EMAIL_SUBJECT_TEXT = "Contacting Nemo...";

const String MAP_SHARED_PRED_LATITUDE_KEY = 'latitude';
const String MAP_SHARED_PRED_LONGITUDE_KEY = 'longitude';
const String MAP_SHARED_PRED_USER_TIMESTAMP_KEY = 'user-timestamp';
const String MAP_SHARED_PRED_CHILD_TIMESTAMP_KEY = 'child-timestamp';
const String MAP_SHARED_PRED_GROUP_TIMESTAMP_KEY = 'group-timestamp';
const String MAP_SHARED_PRED_TRIP_TIMESTAMP_KEY = 'trip-timestamp';
const String MAP_SHARED_PRED_PICKUP_DROPOFF_TIMESTAMP_KEY =
    'pickup-dropoff-timestamp';
const String LOCALE_SP_KEY = 'deviceLocale';
const String MAP_SHARED_PRED_SCHOOL_TIMESTAMP_KEY = 'school-timestamp';

// Shared Preferences Key
const String CURRENT_PHONE_NUMBER_SP_KEY = 'currentPhoneNumber';
const String CURRENT_USER_ID_SP_KEY = 'currentUserId';
const String GET_CHILDS_SP_KEY = 'getChilds';
const String GET_CHILD_GROUPS_SP_KEY = 'getChildGroups';
const String GET_PICKUP_DROPOFF_SP_KEY = 'getPickupDropoffs';
const String GET_LATEST_TRIPS_SP_KEY = 'getLatestTrips';
const String GET_USER_TYPE_SP_KEY = 'getUserType';
const String DEVICE_ID_SP_KEY = 'deviceId';

const String FIRESTORE_USER_KEY = 'user';
const String FIRESTORE_CHILD_KEY = 'child';
const String FIRESTORE_GROUP_KEY = 'group';
const String FIRESTORE_TRIP_KEY = 'trip';
const String FIRESTORE_PICKUP_DROPOFF_KEY = 'pickupDropOff';
const String MENU = 'Menu';
const String MAP_SHARED_PRED_ADDRESS_TIMESTAMP_KEY = 'address-timestamp';
const String FIRESTORE_SCHOOL_KEY = 'school';
const String FIRESTORE_ADDRESS_KEY = 'address';

///Firebase Storage Route path
const String PROFILE_IMAGES = "profile_images/";
const String CHILD_PROFILE_IMAGES = "child_profile_images/";

///Form Field Attributes and validators
const String PHONE_NUMBER_FIELD_ATTRIBUTE = 'phoneNumber';
const String NAME_FIELD_ATTRIBUTE = 'name';
const String EMAIL_FIELD_ATTRIBUTE = "email";
const String PHONE_NUMBER_FORMAT_VALIDATOR = '00000 00000';
const String PHONE_NUMBER_KEY = 'EnterPhone-TextFormField';
const String NAME_FIELD_MAX_VALIDATOR = '##############################';

/// Hero tags
const String DRAWER_UPDATE_PROFILE_HERO_TAG = "profile-image";
const String CHILD_PROFIL_IMAGE_HERO_TAG = "child-profile-image";

//Analytics Strings
const String UPLOAD_IMAGE_ICON = "upload_photo_icon";
const String UPDATE_PROFILE_SCREEN = "update_profile_screen";
const String PROFILE_EDIT_SAVE_BTN = "profile_edit_save_button";
const String PROFILE_EDIT_CANCEL_BTN = "profile_edit_cancel_button";
const String EMAIL_EDIT_CANCEL_BTN = "email_edit_cancel_button";
const String EMAIL_EDIT_SAVE_BTN = "email_edit_save_button";
const String EDIT_NAME_ICON = "edit_name_icon";
const String EDIT_EMAIL_ICON = "edit_email_icon";
const String EDIT_ADDRESS_ICON = "edit_address_icon";
const String CLEAR_OTP_BTN = "clear_OTP_button";
const String ENTER_OTP_SCREEN = "enter_OTP_PIN_screen";
const String MAP_HOME = "map_home";
const String ENTER_PHONE_NUMBER_SCREEN = "enter_phone_number_screen";
const String LOADING_SCREEN = "loading_screen";
const String SIGN_OUT_BTN = "sign_out_button";
const String TERM_CONDITIONS_BTN = "term_conditions_button";
const String PRIVACY_POLICY_BTN = "privacy_policy_button";
const String OPEN_SOURCE_BTN = "open_source_button";
const String EMAIL_APP_LAUNCHER_BTN = "email_app_launcher_icon";
const String DRAWER_SCREEN = "drawer_screen";
const String REFRESH_BTN = "refresh_button";
const String NO_GROUPS_SCREEN = "no_groups_screen";
const String NO_INTERNET_SCREEN = "no_internet_screen";
const String CREATE_PROFILE_SCREEN = "create_profile_screen";
const String CREATE_CHILD_SCREEN = "create_child_screen";
const String SEND_OTP_BTN = 'send_OTP_button';
const String UPLOAD_PHOTO_GALLERY = "upload_photo_galleryOption";
const String UPLOAD_PHOTO_CAMERA = "upload_photo_cameraOption";
const String LANGUAGE_SAVE_BTN = 'language_save_button';
const String LANGUAGE_BTN = 'language_button';
const String ABOUT_BTN = 'about_button';
const String CHILD_BTN = 'child_button';
const String CREATE_USER_NEXT_BTN = "create_user_next_button";
const String CREATE_USER_UPLOAD_IMAGE_ICON = "create_user_upload_photo_icon";
const String CREATE_CHILD_UPLOAD_IMAGE_ICON = "create_user_upload_photo_icon";
const String ADD_PARENT_IMAGE_SKIP_BTN = "skip_button_add_parent_image";
const String CREATE_USER_DONE_BTN = "create_user_done_button";
const String CREATE_CHILD_DONE_BTN = "create_user_done_button";
const String PICK_LOCATION_CONTINUE_BUTTON = "pick_location_continue_button";
const String MAP_ADDRESS_TAP = "map_address_tap";
const String ADD_CHILD_IMAGE_SKIP_BTN = "skip_button_add_child_image";
const String LINK_BY_PUBLIC_GROUP_ID_OPTION = "link_by_group_id_option";
const String LINK_BY_DRIVER_PHONE_OPTION = "link_by_driver_phone_option";
const String GROUP_ID_SEARCH_BUTTON = "group_id_search_button";
const String ADD_CHILD_BY_PUBLIC_ID_TO_SELECTED_GROUP =
    "add_child_by_public_id_to_selected_group";
const String ADD_CHILD_BY_PHONE_TO_SELECTED_GROUP =
    "add_child_by_phone_to_selected_group";
const String ADD_CHILD_TO_GROUP_BY_DYNAMIC_LINK =
    "add_child_to_group_by_dynamic_link";
const String DELETE_CHILD_BTN = "delete_child_dialog";
const String CHILD_DELETE_BTN = "child_delete_confirm_button";
const String CHILD_DELETE_CANCEL_BTN = "child_delete_cancel_button";
const String UNLINK_CHILD_ICON = "unlink_child_icon";
const String UNLINK_CHILD_CONFIRM_BTN = "unlink_child_confirm_button";
const String UNLINK_CHILD_CANCEL_BTN = "unlink_child_cancel_button";
const String TOGGLE_CHILD_CARD = "child_card_toggle";
const String RESET_MAP_VIEW = "reset_map_view";
