String getRandomAvatar(String index) {
  int avatarId = int.parse(index) % 18;
  if (avatarId > 17) {
    avatarId = 0;
  }
  return "assets/images/$avatarId.png";
}

String getTaxiDriverAvatar() {
  return "assets/images/navigation-arrow.png";
}
