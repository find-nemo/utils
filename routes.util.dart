import 'package:flutter/widgets.dart';
import 'package:flutter/material.dart';
import 'package:nemoparent/components/license.component.dart';
import 'package:nemoparent/pages/about.page.dart';
import 'package:nemoparent/pages/child_check.page.dart';
import 'package:nemoparent/pages/child_profile.page.dart';
import 'package:nemoparent/pages/create_child.page.dart';
import 'package:nemoparent/pages/create_profile.page.dart';
import 'package:nemoparent/pages/home.page.dart';
import 'package:nemoparent/pages/language.page.dart';
import 'package:nemoparent/pages/link_by_driver_phone.page.dart';
import 'package:nemoparent/pages/link_by_dynamic_link.page.dart';
import 'package:nemoparent/pages/link_by_public_group_id.page.dart';
import 'package:nemoparent/pages/login.page.dart';
import 'package:nemoparent/pages/otp.page.dart';
import 'package:nemoparent/pages/dashboard.page.dart';
import 'package:nemoparent/pages/custom_webview.page.dart';
import 'package:nemoparent/pages/pick_location.page.dart';
import 'package:nemoparent/pages/profile_check.page.dart';
import 'package:nemoparent/pages/update_profile.page.dart';

const String UPDATE_USER_PROFILE_PAGE_ROUTE = '/update_user_profile';

const String DEFAULT_PAGE_ROUTE = '/';
const String HOME_PAGE_ROUTE = '/home';
const String LOGIN_PAGE_ROUTE = '/login';
const String OTP_PAGE_ROUTE = '/otp';
const String DASHBOARD_PAGE_ROUTE = '/dashboard';
const String CUSTOM_WEB_VIEW_PAGE_ROUTE = '/custom_web_view';
const String LANGUAGE_UPDATE_PAGE_ROUTE = '/language_update';
const String CAPTURE_PROFILE_IMAGE_PAGE_ROUTE = '/capture_profile_image';
const String CHILD_PROFILE_PAGE_ROUTE = '/child_profile';
const String CREATE_UPDATE_CHILD_PROFILE_PAGE_ROUTE = '/create_child_profile';
const String CHILD_CHECK_PAGE_ROUTE = '/child_check';
const String PICK_LOCATION_PAGE_ROUTE = '/pick_location';
const String CREATE_USER_PROFILE_PAGE_ROUTE = '/create_user_profile';
const String PROFILE_CHECK_PAGE_ROUTE = '/profile_check';
const String CAPTURE_CHILD_IMAGE_PAGE = '/capture_child_image';
const String LINK_BY_GROUP_ID = '/link_by_group_id';
const String LINK_BY_DRIVER_PHONE = '/link_by_driver_phone';
const String LINK_BY_DYNAMIC_LINK = '/link_by_dynamic_link';
const String LICENSE = '/license';
const String ABOUT_PAGE_ROUTE = '/about_page';

class RouteGenerator {
  static Route<dynamic> generateRoute(RouteSettings settings) {
    switch (settings.name) {
      case DEFAULT_PAGE_ROUTE:
      case HOME_PAGE_ROUTE:
        return MaterialPageRoute(
            builder: (_) => HomePage(),
            settings: RouteSettings(
                name: settings.name, arguments: settings.arguments));
      case LOGIN_PAGE_ROUTE:
        return MaterialPageRoute(
            builder: (_) => LoginPage(),
            settings: RouteSettings(
                name: settings.name, arguments: settings.arguments));
      case ABOUT_PAGE_ROUTE:
        return MaterialPageRoute(
            fullscreenDialog: true,
            builder: (_) => AboutPage(),
            settings: RouteSettings(
                name: settings.name, arguments: settings.arguments));
      case OTP_PAGE_ROUTE:
        return MaterialPageRoute(
            builder: (_) => OtpPage(),
            settings: RouteSettings(
                name: settings.name, arguments: settings.arguments));
      case CUSTOM_WEB_VIEW_PAGE_ROUTE:
        return MaterialPageRoute(
            builder: (_) => CustomWebView(settings.arguments),
            settings: RouteSettings(
                name: settings.name, arguments: settings.arguments));
      case UPDATE_USER_PROFILE_PAGE_ROUTE:
        return MaterialPageRoute(
            builder: (_) => UpdateUserProfilePage(),
            settings: RouteSettings(
                name: settings.name, arguments: settings.arguments));
      case LANGUAGE_UPDATE_PAGE_ROUTE:
        return MaterialPageRoute(
            builder: (_) => LanguageUpdatePage(),
            settings: RouteSettings(
                name: settings.name, arguments: settings.arguments));
      case CAPTURE_PROFILE_IMAGE_PAGE_ROUTE:
        return MaterialPageRoute(
            builder: (_) => CaptureProfileImagePage(),
            settings: RouteSettings(
                name: settings.name, arguments: settings.arguments));
      case CHILD_PROFILE_PAGE_ROUTE:
        return MaterialPageRoute(
            builder: (_) => ChildProfilePage(),
            settings: RouteSettings(
                name: settings.name, arguments: settings.arguments));
      case CREATE_UPDATE_CHILD_PROFILE_PAGE_ROUTE:
        return MaterialPageRoute(
            fullscreenDialog: true,
            builder: (_) => CreateUpdateChildPage(),
            settings: RouteSettings(
                name: settings.name, arguments: settings.arguments));
      case CHILD_CHECK_PAGE_ROUTE:
        return MaterialPageRoute(
            builder: (_) => ChildCheckPage(),
            settings: RouteSettings(
                name: settings.name, arguments: settings.arguments));
      case PICK_LOCATION_PAGE_ROUTE:
        return MaterialPageRoute(
            builder: (_) => PickLocationPage(),
            settings: RouteSettings(
                name: settings.name, arguments: settings.arguments));
      case CREATE_USER_PROFILE_PAGE_ROUTE:
        return MaterialPageRoute(
            builder: (_) => CreateUpdateUserProfilePage(),
            settings: RouteSettings(
                name: settings.name, arguments: settings.arguments));
      case PROFILE_CHECK_PAGE_ROUTE:
        return MaterialPageRoute(
            builder: (_) => ProfileCheckPage(),
            settings: RouteSettings(
                name: settings.name, arguments: settings.arguments));
      case CAPTURE_CHILD_IMAGE_PAGE:
        return MaterialPageRoute(
            builder: (_) => CaptureChildImagePage(),
            settings: RouteSettings(
                name: settings.name, arguments: settings.arguments));
      case DASHBOARD_PAGE_ROUTE:
        return MaterialPageRoute(
            builder: (_) => DashboardPage(),
            settings: RouteSettings(
                name: settings.name, arguments: settings.arguments));
      case LINK_BY_GROUP_ID:
        return MaterialPageRoute(
            builder: (_) => LinkPublicGroupByID(),
            fullscreenDialog: true,
            settings: RouteSettings(
                name: settings.name, arguments: settings.arguments));
      case LINK_BY_DRIVER_PHONE:
        return MaterialPageRoute(
            builder: (_) => LinkGroupByDriverPhone(),
            fullscreenDialog: true,
            settings: RouteSettings(
                name: settings.name, arguments: settings.arguments));
      case LINK_BY_DYNAMIC_LINK:
        return MaterialPageRoute(
            fullscreenDialog: true,
            builder: (_) => LinkGroupByDynamicLink(),
            settings: RouteSettings(
                name: settings.name, arguments: settings.arguments));
      case LICENSE:
        final typeArg = settings.arguments as LicensePageArguments;
        return MaterialPageRoute(
            fullscreenDialog: true,
            builder: (_) => License(
                  header: typeArg.header,
                  mdFileName: typeArg.mdFileName,
                ),
            settings: RouteSettings(
                name: settings.name, arguments: settings.arguments));
      default:
        return MaterialPageRoute(
          builder: (_) => Scaffold(
            appBar: AppBar(
              title: Text('ERROR'),
              centerTitle: true,
            ),
            body: Center(
              child: Text(
                  'Page not found!\nNo route defined for ${settings.name}'),
            ),
          ),
        );
    }
  }
}

class LicensePageArguments {
  final String header;
  final String mdFileName;

  LicensePageArguments({@required this.header, @required this.mdFileName});
}
