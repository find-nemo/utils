import 'package:nemoparent/app_config.dart';
import 'package:openapi/api.dart';

class PresentationApiClient {
  // Singleton
  static final PresentationApiClient _presentationApiClient =
      PresentationApiClient._internal();

  factory PresentationApiClient() => _presentationApiClient;

  // private constructor
  PresentationApiClient._internal();

  static PresentationApiClient get shared => _presentationApiClient;

  String accessToken;

  ApiClient getUserApiClient() {
    return ApiClient(basePath: Config.userApiUrl, accessToken: accessToken);
  }

  ApiClient getDeviceApiClient() {
    return ApiClient(basePath: Config.deviceApiUrl, accessToken: accessToken);
  }

  ApiClient getGroupApiClient() {
    return ApiClient(basePath: Config.groupApiUrl, accessToken: accessToken);
  }
}
