import 'package:intl/intl.dart';

String formatChildCardDate(String dateTime) {
  final template = DateFormat('MMM d, h:mm a');
  final timestamp = DateTime.parse(dateTime ?? DateTime.now());
  return template.format(timestamp.toLocal());
}

String formatArrivalTime(int millisecondsSinceEpoch, int seconds) {
  final template = DateFormat.jm();
  final timestamp = DateTime.fromMillisecondsSinceEpoch(millisecondsSinceEpoch);
  return template.format(timestamp.add(Duration(seconds: seconds)));
}

int getEpochTime(String dateTime) {
  final timestamp = DateTime.parse(dateTime ?? DateTime.now());
  return timestamp.millisecondsSinceEpoch;
}
