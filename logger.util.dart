import 'dart:async';

import 'package:f_logs/f_logs.dart';
import 'package:firebase_crashlytics/firebase_crashlytics.dart';
import 'package:nemoparent/app_config.dart';
import 'package:nemoparent/utils/constants.util.dart';

class LoggingHelper {
  // Singleton
  static final LoggingHelper _loggingHelper = LoggingHelper._internal();

  static final Crashlytics _crashlytics = Crashlytics();

  factory LoggingHelper() => _loggingHelper;

  // private constructor
  LoggingHelper._internal();

  LoggingHelper.instance() {
    processLog();
    Timer.periodic(Duration(seconds: PROVIDER_REFRESH_SECONDS * 4),
        (Timer t) => processLog());
  }

  static LoggingHelper get shared => _loggingHelper;

  void addLog(String message) {
    _crashlytics.log(message);
  }

  Future<void> processLog() async {
    if (Config.appFlavor == Flavor.PROD) {
      List<Log> filterLogs = await FLog.getAllLogs();
      FLog.clearLogs();
      if (filterLogs != null && filterLogs.length > 0) {
        filterLogs.forEach((element) {
          addLog(element.toString());
        });
      }
    } else {
      FLog.clearLogs();
    }
  }
}
