import 'dart:io';

import 'package:f_logs/f_logs.dart';
import 'package:firebase_messaging/firebase_messaging.dart';

class PushNotificationService {
  static final PushNotificationService _pushNotificationService =
      PushNotificationService._internal();

  factory PushNotificationService() => _pushNotificationService;

  PushNotificationService._internal();

  PushNotificationService get shared => _pushNotificationService;

  final FirebaseMessaging _fcm = FirebaseMessaging();
  String fcmToken;

  Future initialise() async {
    try {
      if (Platform.isIOS) {
        _fcm.requestNotificationPermissions(IosNotificationSettings());
      }

      _fcm.getToken().then((value) {
        fcmToken = value;
      }).catchError((onError) =>
          FLog.logThis(text: onError.toString(), type: LogLevel.ERROR));

      _fcm.configure(
        onMessage: (Map<String, dynamic> message) async {
          print("onMessage: $message");
        },
        onLaunch: (Map<String, dynamic> message) async {
          print("onLaunch: $message");
        },
        onResume: (Map<String, dynamic> message) async {
          print("onResume: $message");
        },
      );
    } catch (e) {
      FLog.logThis(text: e.toString(), type: LogLevel.ERROR);
      return null;
    }
  }
}
